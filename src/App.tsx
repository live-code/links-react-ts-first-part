import React from 'react';
import './index.css';
import UsersPage from './views/users/UsersPage';
import { ContactsPage } from './views/contacts/ContactsPage';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { PageLogin } from './views/login/LoginPage';
import { Navbar } from './core/components/Navbar';
import { PageAdmin } from './views/admin/PageAdmin';
import { ContactsDetailsPage } from './views/contacts-details/ContactsDetailsPage';
import { PrivateRoute } from './core/auth/PrivateRoute';
import { isLogged } from './core/auth/authentication.service';


const App = () => {

  return (
    <BrowserRouter>

      <Navbar />

      <div className="container mt-3">
        <Switch>
          <Route path="/login" component={PageLogin} />

          <Route path="/users">
            <UsersPage />
          </Route>

          <PrivateRoute path="/admin">
            <PageAdmin/>
          </PrivateRoute>

          <Route path="/contacts" exact>
            <ContactsPage />
          </Route>

          <Route path="/contacts/:id" component={ContactsDetailsPage} />

          <Route path="*">
            <Redirect to="/login" />
          </Route>
        </Switch>
      </div>
      {/*<ContactsPage />*/}
      {/*<UsersPage />*/}
    </BrowserRouter>
  )
};

export default App;


