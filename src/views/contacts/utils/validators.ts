import { FormAddPerson } from '../model/person';

const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
export function validateEmail(emailAddress: string): boolean {
  return re.test(emailAddress.toLowerCase())
}


export function validateAll(data: FormAddPerson) {
  const isValidUsername = data.username.length > 3;
  const isValidEmail = validateEmail(data.email)
  const valid = isValidUsername && isValidEmail;
  return { isValidUsername, isValidEmail, valid  }
}
