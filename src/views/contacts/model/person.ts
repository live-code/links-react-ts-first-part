export interface Person {
  id: number;
  username: string;
  email: string;
}

export type FormAddPerson = Omit<Person, 'id'>;
