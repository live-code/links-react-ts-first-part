import React  from 'react';
import { Person } from '../model/person';
import { NavLink } from 'react-router-dom';

interface ContactsListProps {
  selectedPerson: Person | null;
  persons: Person[];
  onGotoDetails: (id: number) => void;
  onSelect: (person: Person) => void;
  onDelete: (id: number) => void;
}

export const ContacstList: React.FC<ContactsListProps> = (props) => {

  const deleteHandler = (id: number, event: React.MouseEvent) => {
    event.stopPropagation();
    props.onDelete(id)
  }

  return <div>
    {
      props.persons.map(p => {
        return <li
          key={p.id}
          onClick={() => props.onSelect(p)}
          style={{
            color: p.id === props.selectedPerson?.id ? 'red' : 'black'
          }}
        >
          {p.username}

          <div className="pull-right">
            <i
              className="fa fa-trash"
              onClick={(e) => deleteHandler(p.id, e)}
            />

            <i className="fa fa-info-circle" onClick={() => props.onGotoDetails(p.id)} />

            <NavLink to={'contacts/' + p.id}>
              <i className="fa fa-info-circle" />
            </NavLink>
          </div>
        </li>
      })
    }
  </div>
};
