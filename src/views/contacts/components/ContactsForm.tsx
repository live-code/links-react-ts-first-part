import React, { useEffect, useState } from 'react';
import cn from 'classnames';

import { FormAddPerson, Person } from '../model/person';
import { validateAll } from '../utils/validators';

const INITIAL_STATE: FormAddPerson = { username: '', email: ''};

interface ContactsFormsProps {
  selectedPerson: Person | null;
  onReset: () => void;
  onSave: (data: FormAddPerson) => void;
}

const ContactsFormsX: React.FC<ContactsFormsProps> = (props) => {
  const [formData, setFormData] = useState<FormAddPerson>(INITIAL_STATE);
  const [dirty, setDirty] = useState(false);

  useEffect(() => {
    if (props.selectedPerson) {
      setFormData(props.selectedPerson);
    } else {
      setFormData(INITIAL_STATE);
    }
  }, [props.selectedPerson]);

  function onSubmitHandler(e: React.FormEvent<HTMLFormElement>) {
    console.log(formData)
    e.preventDefault();
    setDirty(false);
    props.onSave(formData);
  }


  function onChangeHandler(e: React.ChangeEvent<HTMLInputElement>) {
    const propName = e.currentTarget.name;
    const value = e.currentTarget.value;
    setDirty(true);
    setFormData({
      ...formData,
      [propName]: value
    })
  }

  const { isValidUsername, isValidEmail, valid } = validateAll(formData)

  function resetHandler() {
    setFormData(INITIAL_STATE);
    setDirty(false)
    props.onReset();
  }

  return (
    <form onSubmit={onSubmitHandler}>
      <input
        type="text"
        className={cn(
          'form-control',
          { 'is-valid': isValidUsername},
          { 'is-invalid': !isValidUsername && dirty}
        )}
        placeholder="Write your name"
        name="username"
        onChange={onChangeHandler}
        value={formData.username}
      />
      <input
        type="text"
        className={cn(
          'form-control',
          { 'is-valid': isValidEmail},
          { 'is-invalid': !isValidEmail && dirty}
        )}
        placeholder="Write your email"
        name="email"
        onChange={onChangeHandler}
        value={formData.email}
      />

      <button type="submit" disabled={!valid}>SAVE</button>
      <button type="button" onClick={resetHandler}>RESET</button>
    </form>
  )
};


export const ContactsForms = React.memo(ContactsFormsX)
