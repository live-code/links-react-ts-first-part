import { useEffect, useState } from 'react';
import { FormAddPerson, Person } from '../model/person';
import axios from 'axios';

export function useContacts() {
  const [selectedPerson, setSelectedPerson] = useState<Person | null>(null)
  const [persons, setPersons] = useState<Person[]>([])

  useEffect(() => {
    axios.get('http://localhost:3001/users')
      .then(res => setPersons(res.data))
  }, [])


  // function submitForm(data: Partial<Person>) {
  function submitForm(data: Person | FormAddPerson) {
    if ('id' in data) {
      editHandler(data as Person)
    } else {
      addHandler(data as FormAddPerson)
    }
  }

  function editHandler(data: Person) {
    const { id, ...rest } = data;
    axios.patch<Person>('http://localhost:3001/users/' + data.id, rest)
      .then(() => {
        setPersons(
          persons.map(p => p.id === data.id ? data : p)
        )
      })
  }

  function addHandler(data: FormAddPerson) {
    axios.post<Person>('http://localhost:3001/users/', data)
      .then((res) => {
        setPersons([...persons, res.data]);
        setSelectedPerson(res.data);
      })
  }

  function deleteHandler(id: number) {
    console.log(id, selectedPerson?.id)
    axios.delete('http://localhost:3001/users/' + id)
      .then(() => {
        setPersons(persons.filter(p => p.id !== id));
        if (id === selectedPerson?.id) {
          setSelectedPerson(null);
        }
      })
  }

  function onSelect(person: Person) {
    setSelectedPerson(person)
  }

  function onResetHandler() {
    setSelectedPerson(null)
  }

  return {
    selectedPerson,
    persons,
    actions: {
      save: submitForm,
      del: deleteHandler,
      select: onSelect,
      reset: onResetHandler,
    },
  }
}
