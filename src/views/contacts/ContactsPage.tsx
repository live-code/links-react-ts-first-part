import React from 'react';
import { ContactsForms } from './components/ContactsForm';
import { ContacstList } from './components/ContactsList';
import { useContacts } from './hooks/contacts.hook';
import { useHistory } from 'react-router-dom';

export const ContactsPage: React.FC = () => {
  const { selectedPerson, persons, actions  } = useContacts();
  const history = useHistory();

  const goto = (id: number) => {
    history.push('contacts/' + id);
  }
  ;
  return (
    <div className="centered">

      <ContactsForms
        selectedPerson={selectedPerson}
        onSave={actions.save}
        onReset={actions.reset}
      />

      <ContacstList
        selectedPerson={selectedPerson}
        onGotoDetails={goto}
        persons={persons}
        onSelect={actions.select}
        onDelete={actions.del}
      />

    </div>
  )
}
