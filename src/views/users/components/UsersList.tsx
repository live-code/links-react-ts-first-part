import React from 'react';
import { User } from '../model/user';
import cn from 'classnames';

interface UsersListProps {
  selectedId: number;
  users: User[];
  onSelect: (id: number) => void;
  onDelete: (id: number) => void;
}

export const UserList: React.FC<UsersListProps> = props => {
  return <>
    {
      props.users.map(u => {
        return (
          <li
            className={cn(
             'list-group-item',
             {'active': u.id === props.selectedId}
            )}
            key={u.id}
            onClick={() => props.onSelect(u.id)}
          >
            {u.name}

            <div className="pull-right">
              <i className="fa fa-trash"
                 onClick={() => props.onDelete(u.id)} />
            </div>
          </li>)
      })
    }
  </>
}

