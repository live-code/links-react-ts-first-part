
// UserDetails.tsx
import { User } from '../model/user';
import React from 'react';

interface UserDetailsProps {
  user?: User;
  onNextButtonClick: () => void
}

export const UsersDetails: React.FC<UserDetailsProps> = ({ user, onNextButtonClick }) => {
  return (
    <div className="card">
      <div className="card-header">
        {user?.name} - { user?.id}
      </div>

      <div className="card-body">
        {user?.website}

      </div>

      <UserDetailsFooter onNextButtonClick={onNextButtonClick}/>
    </div>
  )
}


// UserDetailsBody
interface UserDetailsBodyProps {
  user?: User;
  onNextButtonClick: () => void
}

export const UserDetailsFooter: React.FC<UserDetailsBodyProps> = props => {
  return <div className="card-footer">
    <button onClick={props.onNextButtonClick}>Next</button>
  </div>
}
