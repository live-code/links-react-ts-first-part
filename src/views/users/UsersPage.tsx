import { useUserDetails } from './hooks/users.hook';
import { UsersDetails } from './components/UsersDetails';
import React, { useEffect, useState } from 'react';
import { User } from './model/user';
import { UserList } from './components/UsersList';
import { get } from '../../core/auth/auth.interceptor';


const UsersPage = () => {
  const { user, nextUser, id, setId } = useUserDetails();
  const [ users, setUsers ] = useState<User[]>([]);

  useEffect(() => {
    fetch('https://jsonplaceholder.typicode.com/users')
      .then(res => res.json())
      .then(res => setUsers(res))

    get<User[]>('https://jsonplaceholder.typicode.com/users')
      .then(res => setUsers(res))

  }, [])

  const deleteHandler = (id: number) => {
    fetch(`https://jsonplaceholder.typicode.com/users/${id}`, { method: 'DELETE'})
      .then(res => res.json())
      .then(() => {
        setUsers(
          users.filter(u => u.id !== id)
        )
      })
  }

  return (
    <>
      <div className="centered">

        <UserList
          selectedId={id}
          users={users}
          onDelete={deleteHandler}
          onSelect={setId}
        />

        <hr/>

        {
          user &&
          <UsersDetails
            user={user}
            onNextButtonClick={nextUser} />
        }
      </div>
    </>
  )
};

export default UsersPage;
