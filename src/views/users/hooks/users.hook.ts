import { useEffect, useState } from 'react';
import { User } from '../model/user';

export function useUserDetails() {
  const [id, setId] = useState<number>(1)
  const [user, setUser] = useState<User | null>(null)

  useEffect(() => {
    fetch(`https://jsonplaceholder.typicode.com/users/${id}`)
      .then(res => res.json())
      .then(res => setUser(res))
  }, [id]);


  const next = () => {
    if (id < 10) {
      setId(id + 1)
    } else {
      setId(1)
    }
  }

  return { user, id, setId, nextUser: next };
}
