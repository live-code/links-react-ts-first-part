import React, { useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import axios from 'axios';
import { Person } from '../contacts/model/person';

interface RouteParams {id: string }

export const ContactsDetailsPage: React.FC<RouteComponentProps<RouteParams>> = (props) => {
  const [user, setUser] = useState<Person | null>(null)
  useEffect(()=> {
    axios.get<Person>('http://localhost:3001/users/' + props.match.params.id)
      .then(res => setUser(res.data))
  }, [])

  return <div>
    Name: {user?.username}
    <hr/>
    Email: {user?.email}
    <hr/>

  </div>
};
