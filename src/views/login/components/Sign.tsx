import React  from 'react';
import { Credentials, signIn } from '../../../core/auth/authentication.service';
import { useHistory } from 'react-router-dom';

export const SignIn: React.FC = () => {
  const history = useHistory()

  const login = () => {
    const c: Credentials = { username: '123', password: 'abc'};
    signIn(c).then((res) => {
      console.log(res.data.token)
      history.push('users');
    })
  };

  return <div>
    <h1>SignIn</h1>
    <input type="text" className="form-control"/>
    <input type="text" className="form-control"/>

    <button onClick={login}>Login</button>
  </div>
};
