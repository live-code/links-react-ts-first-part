import React, { useState } from 'react';
import { LostPass } from './components/LostPass';
import { Registration } from './components/Registration';
import { SignIn } from './components/Sign';
import { NavLink, RouteComponentProps, Route, Redirect } from 'react-router-dom';

function BreadCrumbs(props: RouteComponentProps<{
  sectionName: string
}>) {
  return <nav aria-label="breadcrumb">
    <ol className="breadcrumb">
      <li className="breadcrumb-item">Login</li>
      <li className="breadcrumb-item active" aria-current="page">
        {props.match.params.sectionName}
      </li>
    </ol>
  </nav>
}

export const PageLogin: React.FC<RouteComponentProps> = (props ) => {
  const { path, url } = props.match;

  return <div>

    <Route path={`${path}/:sectionName`} component={BreadCrumbs} />


    <Route path="/login/signin">
      <SignIn />
    </Route>

    <Route path="/login/registration">
      <Registration />
    </Route>

    <Route path="/login/lostpass">
      <LostPass />
    </Route>

    <Route path={url} exact>
      <div className="card">
        <div className="card-header">
          welcome to LINKS
        </div>
        <div className="card-body">
          <NavLink to="/login/signin">Vai al login</NavLink>
        </div>
      </div>
    </Route>


    <hr/>
    <div className="btn-group">
      <NavLink className="btn btn-link" to='/login/signin' activeClassName="text-dark">signin</NavLink>
      <NavLink className="btn btn-link" to={`${path}/registration`} activeClassName="text-dark">reg</NavLink>
      <NavLink className="btn btn-link" to={`${path}/lostpass`} activeClassName="text-dark">Lost pass</NavLink>
    </div>

  </div>
};
