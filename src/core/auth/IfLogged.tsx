import React  from 'react';
import { isLogged } from './authentication.service';

export const IfLogged: React.FC<any> = ({children}) => {
  return isLogged() ? <>{children}</> : null;
};
