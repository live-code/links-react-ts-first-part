import React  from 'react';
import { Redirect, Route } from 'react-router-dom';
import { isLogged } from './authentication.service';

interface PrivateRouteProps {
  path: string;
}

export const PrivateRoute: React.FC<PrivateRouteProps> = ({children, ...rest}) => {
  return isLogged() ? <Route {...rest}>
    {children}
  </Route> : <Redirect to="/login" />
};
