import axios, { AxiosResponse } from 'axios';
import { Auth } from './auth';
import { clear, getItem, setItem } from './localstorage.helper';

export interface Credentials {
  username: string;
  password: string;
}
export async function signIn(credential: Credentials): Promise<AxiosResponse<Auth>> {
  const response: AxiosResponse<Auth> = await axios.get('http://localhost:3001/login');
  console.log(response.data.token)

  setItem('token', response.data.token)
  return response;
}


export function signOut() {
  clear('token');
}

export function isLogged(): boolean {
  return !!getItem('token')
}
