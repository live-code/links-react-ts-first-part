import React, { Fragment, useEffect, useState } from 'react';
import { NavLink, useHistory } from 'react-router-dom';
import { isLogged, signOut } from '../auth/authentication.service';
import { IfLogged } from '../auth/IfLogged';
import axios from 'axios';

// export const views: string[] = ['login', 'users', 'contacts', 'admin'];
/*export const views: any[] = [
  { path: 'login', label: 'Login', protected: false },
  { path: 'users', label: 'Users', protected: true },
  { path: 'contacts', label: 'contacts', protected: false },
  { path: 'admin', label: 'admin', protected: true },
];*/

export const Navbar: React.FC = () => {
  const [views, setViews] = useState<any[]>([])
  const history = useHistory();

  useEffect(() => {
    axios.get('http://localhost:3001/menu')
      .then(res => setViews(res.data))
  }, [])

  function signoutHandler() {
    signOut();
    history.push('login')
  }

  function renderComponent(menuItem: any) {
    // { v.kind === 'chart' ? <Chart data={v.data} /> : <Panel title={v.label} />{v.path}</Panel }
    switch(menuItem.kind) {
      case 'BarChart':
        // parsing dei dati
        return "<Chart config={menuItem.data.series} legend={menuItem.data.legend} />";

      case 'Card':
        // parsing dei dati
        return "<Card title={menuItem.data.title}>{menuItem.data.desc}</Card>";


    }
  }

  return <div>
    <ul className="nav bg-dark">

      {
        views.map((v, index) => {
          const menuItem = (
            <li className="nav-item">
              <NavLink
                to={`/${v.path}`}
                className="nav-link text-white"
                activeClassName="bg-warning text-dark">

                { v.icon ? <i className={'fa fa-' + v.icon} /> : v.label}

                {/*renderComponent(v);*/}
              </NavLink>
            </li>
          );

          return v.protected ? <IfLogged key={index}>{menuItem}</IfLogged> : <Fragment key={index}>{menuItem}</Fragment>;
        })
      }

      <IfLogged>
        <li className="nav-item" onClick={signoutHandler}>
            <span className="nav-link text-white">Logout</span>
        </li>
      </IfLogged>

    </ul>

  </div>
};



/*
export const Navbar: React.FC = () => {
  return <div>
    <ul className="nav">
      <li className="nav-item">
        <NavLink to="/login" className="nav-link" activeClassName="bg-warning">Login</NavLink>
      </li>
      <li className="nav-item">
        <NavLink to="/users" className="nav-link" activeClassName="bg-warning">users</NavLink>
      </li>

      <li className="nav-item">
        <NavLink to="/contacts" exact className="nav-link" activeClassName="bg-warning">contacts</NavLink>
      </li>
      <li className="nav-item">
        <NavLink to="/admin" className="nav-link" activeClassName="bg-warning">admin</NavLink>
      </li>
    </ul>

  </div>
};
*/
